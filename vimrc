let g:pathogen_disabled = ["LaTeX-Box"]
execute pathogen#infect()
set modeline
set ci
set foldlevelstart=0
set foldminlines=5
set nu
set rnu
set t_Co=256

" netrw
let g:netrw_banner=0
let g:netrw_liststyle=3
let g:netrw_altv=1

" vim-ailine
set ls=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_theme='dark'

" UltiSnips
let g:UltiSnipsExpandTrigger = '<c-j>'

" youcompleteme
let g:ycm_filetype_blacklist={'ruby' : 1}
"let g:ycm_key_list_select_completion=[]
"let g:ycm_key_list_previous_completion=[]
"let g:ycm_key_invoke_completion = '<C-b>'

" FastFold
let g:fastfold_savehook = 1
let g:tex_fold_enabled=1

" Invisible characters
set listchars=tab:▸\ ,eol:¬,trail:‧
" \l toggles invisible chars
nmap <leader>l :set list!<CR>

" Turn on hlsearch
set hlsearch

" Key bindings
let debug_gap=1
noremap <f3> <esc>:bprev<CR>
noremap <f4> <esc>:bnext<CR>
noremap <f9> <esc>:w<CR>:make<CR>
nnoremap gb :bn<CR> 
nnoremap gB :bp<CR> 
nnoremap <leader>r :set relativenumber!<CR>

if has('gui_running')
  set background=dark
  let g:solarized_diffmode='high'
  colorscheme solarized
  set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 13
  set guicursor+=n-v-c:blinkon0
  set guicursor+=i:ver25-blinkon0
  set guioptions-=m
  set guioptions-=T
  set guioptions-=r
else
  let g:solarized_termcolors=256
  let g:solarized_visibility='normal'
  "let g:solarized_diffmode='high'
  "let g:solarized_hitrail=0

  "let g:solarized_degrade=1
  set background=dark
  colorscheme solarized
endif
map <S-return> <return>
"map <C-W> :w<CR>
command! -nargs=* Wrap set wrap linebreak nolist
set showcmd
"hi Comment cterm=italic

" Color column
set colorcolumn=+1
" Cursor line
set cul
" Only active winow should have cursorline
augroup BgHighlight
  autocmd!
  autocmd WinEnter * set cursorline
  autocmd WinLeave * set nocursorline
augroup END
"highlight ColorColumn ctermfg=darkred
"call matchadd('ColorColumn', '\%81v',100)

autocmd BufRead *.vala set efm=%f:%l.%c-%[%^:]%#:\ %t%[%^:]%#:\ %m
autocmd BufRead *.vapi set efm=%f:%l.%c-%[%^:]%#:\ %t%[%^:]%#:\ %m
au BufRead,BufNewFile *.vala setfiletype vala
au BufRead,BufNewFile *.vapi setfiletype vala
au BufRead,BufNewFile *.md   set filetype=markdown

filetype plugin indent on
