" my filetype file
if exists("did_load_filetypes")
  finish
endif

augroup filetypedetect
au! BufNewFile,BufRead *
  \ if match(getline(1), '#!/bin/false\|#!/usr/bin/env bash') >= 0 |
  \   set filetype=sh |
  \ endif
au! BufNewFile,BufRead *
  \ if match(getline(1), 'perl') >= 0 |
  \   set filetype=perl |
  \ endif
au! BufNewFile,BufRead *.json set filetype=json
au! BufNewFile,BufRead *.groovy syntax off
augroup END

au! BufNewFile *.pl 0r ~/.vim/skel/perl.skel | normal(G)
au! BufNewFile *.pm 0r ~/.vim/skel/pm.skel
  \ | execute("%s/<>/" . expand('<afile>:t:r') . "/")
  \ | normal(G)
