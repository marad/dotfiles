function! MDFolds()
  let thisline = getline(v:lnum)
  if thisline =~ '^\# '
    return ">1"
  elseif thisline =~ '^## '
    return ">2"
  elseif thisline =~ '^### '
    return ">3"
  elseif thisline =~ '^#### '
    return ">4"
  else
    return "="
  endif
endfunction

setlocal fdm=expr
setlocal foldexpr=MDFolds()
