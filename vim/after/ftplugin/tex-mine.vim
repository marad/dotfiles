"Wrap

function! TeXFolds()
  let thisline = getline(v:lnum)
  " Figures can be nested inside other elements
  if thisline =~ '^\\begin{figure}'
    return "a1"
  elseif match(thisline, '^\\end{figure}') >= 0
    return "s1"
  " Usual markers
  elseif thisline =~ "^% {{{"
    return ">1"
  elseif thisline =~ "^% }}}"
    return "<1"
  else
    return "="
  endif
endfunction

"setlocal foldmethod=marker
setlocal fdm=expr
setlocal foldexpr=TeXFolds()
set tw=79
