setl fdm=indent
setl foldlevel=1
set ts=4
set sts=4
set shiftwidth=4
set expandtab
