"Wrap

function! TeXFolds()
  let thisline = getline(v:lnum)
  " Figures can be nested inside other elements
  "if match(thisline, '\\begin{figure}') >= 0
  if thisline =~ '\\begin{figure}'
    return "a1"
  elseif match(thisline, '\\end{figure}') >= 0
    return "s1"
  " Beamer frames
  "elseif match(thisline, '\\begin{frame}') >= 0
  "  return "a1"
  "elseif match(thisline, '\\end{frame}') >= 0
  "  return "s1"
  "" Bibliography and abstract are always top-level
  "elseif match(thisline, '\\begin{\(thebibliography\|abstract\)}') >= 0
  "  return ">1"
  "elseif match(thisline, '\\end{\(thebibliography\|abstract\)}') >= 0
  "  return "<1"
  " Section levels
  "elseif match(thisline, '\\section{') >= 0
  "  return ">1"
  "elseif match(thisline, '\\subsection{') >= 0
  "  return ">2"
  "elseif match(thisline, '\\subsubsection{') >= 0
  "  return ">3"
  " Usual markers
  elseif thisline =~ "{{{"
    return "a1"
  elseif thisline =~ "}}}"
    return "s1"
  else
    return "="
  endif
endfunction

"setlocal foldmethod=marker
setlocal fdm=expr
setlocal foldexpr=TeXFolds()
set tw=79
