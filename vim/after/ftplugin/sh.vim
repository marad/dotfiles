set ts=4
set sts=4
set shiftwidth=4
set tw=120

" Prevent hard wrapping
set formatoptions-=t
