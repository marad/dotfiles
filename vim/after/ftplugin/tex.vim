" Wrap

function! TeXFolds()
  let thisline = getline(v:lnum)
  " Figures can be nested inside other elements
  if thisline =~ '^\s*\\begin{figure}'
    return "a1"
  elseif match(thisline, '^\s*\\end{figure}') >= 0
    return "s1"
  " Usual markers
  elseif thisline =~ '^[^%]*%[^{]*{{{'
    return 'a1'
  elseif thisline =~ '^[^%]*%[^}]*}}}'
    return 's1'
  " elseif thisline =~ "^% {{{"
  "   return ">1"
  " elseif thisline =~ "^% }}}"
  "   return "<1"
  else
    return "="
  endif
endfunction

function! TeXCommentOut() range
  let [lnum1, col1] = getpos("'<")[1:2]
  let [lnum2, col2] = getpos("'>")[1:2]
  if (getline(lnum1) =~ '\iffalse') && getline(lnum2) =~ '\fi'
    normal! '<dd
    normal! '>dd
    execute "normal! \<c-y>"
  elseif (getline(lnum1-1) =~ '\iffalse') && getline(lnum2+1) =~ '\fi'
    normal! '<kdd
    normal! '>jdd
    execute "normal! \<c-y>"
  else 
    normal! '<O\iffalse
    normal! '>o\fi
    execute "normal! \<c-e>"
  endif
  normal! gv
endfunction

function! TeXCommentOutLine()
  let thisline = getline(line('.'))
  if thisline =~ '%'
    normal! ^x
  else
    normal! ^i%
  endif
endfunction

"setlocal foldmethod=marker
let g:tex_indent_items=0
let g:tex_isk="48-57,a-z,A-Z,192-255,:"
setlocal sts=2
setlocal ts=2
setlocal shiftwidth=2
setlocal expandtab
setlocal fdm=expr
setlocal foldexpr=TeXFolds()
" We remember the fdm and swich to manual in ins mode
"autocmd InsertLeave,WinEnter * let &fdm=g:fdmsave
"autocmd InsertEnter,WinLeave * let g:fdmsave=&fdm | setlocal fdm=manual
"autocmd InsertLeave * let &l:fdm=fdmsave
"autocmd InsertEnter * let fdmsave=&l:fdm | let &l:fdm='manual'
"autocmd InsertLeave *.tex let &l:fdm='expr'
"autocmd InsertEnter *.tex let &l:fdm='manual'
"autocmd InsertEnter * let w:last_fdm=&foldmethod | setlocal foldmethod=manual
"autocmd InsertLeave * let &l:foldmethod=w:last_fdm
vnoremap <silent> <leader>c :call TeXCommentOut()<CR>
nnoremap <silent> <leader>c :call TeXCommentOutLine()<CR>
set tw=79
