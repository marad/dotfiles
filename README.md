# Additional dependencies

* app-vim/pathogen
* app-vim/youcompleteme (git)

# Installation

Run the `install` script. If you only want to reapply the patches, run
`apply-patches`.
